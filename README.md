# Music_Signal_Processing
 
- Pythonを用い，テンポをBPM値により推定(Tempo Estimation) 
- Pythonを用い，音楽からビート及びメロディの発現するタイミングを抽出(Beat/Melody Detection)
- Processingを用い，抽出された結果のmp4による可視化の自動生成
 
# DEMO
 
- ビート・メロディの発現時刻及び各曲のテンポはcsvデータにより出力

- Beat/Melody Detectionとして得られる結果の表示
![detection](./image/detection.png)

- Processingを用いた可視化の例(実際の生成物は音声あり)
![movie](./image/movie.gif)


# Usage
## テンポ推定
music_signal_processing ディレクトリにて，tempo_estimation.py を実行する．
```bash
python tempo_estimation.py
```
## ビート，メロディ検出
music_signal_processing ディレクトリにて，beat_melody_detection.py を実行する．
```bash
python beat_melody_detection.py
```
tempo_estimation.py 内の下記8~17行目を書き換えることで各パラメータの変更が可能
```python
## simulation types
song_length = -1      ## trimming songs from 0 to this sec.  if -1, using full
detect_type = "stft"  ## "moving" or "stft"
filter_type = "hpf"   ## "hpf", "lpf" or "none"

## directory names
data_dir = "input_signal"                       ## mp3 data directory
tempo_dir = "tempo"                             ## tempo.csv directory
visualize_dir = "processingAnimation/src/data"  ## output for processing directory
result_dir = "beat_melody_result"               ## output of detection directory
```

## ビート，メロディのmp4動画生成
music_signal_processing ディレクトリにて，processing_movie.ps1を実行する
```bash
./processing_movie.ps1
```

# I/O
## テンポ推定
### 入力
- /input_signal に置かれた各mp3データ
### 出力
- /tempo/tempo.csvに[曲名, BPM値]の形で表示
## ビート，メロディ検出
### 入力
- /input_signal に置かれた各mp3データ
- /tempo/tempo.csv による各曲テンポデータ(任意)
### 出力
- /beat_melody_result 内「曲名.png」で検出結果の表示
- /beat_melody_result 内「曲名.csv」でメロディ及びビートの発現時刻の表示
- /processingAnimation/src/data 内　動画生成用画像，音声及び検出結果
## ビート，メロディ動画生成
### 入力
- /processingAnimation/src/data 内　動画生成用画像，音声及び検出結果
### 出力
- /processingAnimation/output 内「曲名_BEAT.mp4」あるいは「曲名_MELODY.mp4」によって可視化

# Requirement
## テンポ推定及びビート，メロディ検出

- Python 3.7.6
- numpy 1.19.2
- scipy 1.5.2
- librosa 0.8.0
- matplotlib 3.3.1
- soundfile 0.10.3

Environments under [Anaconda for Windows](https://www.anaconda.com/distribution/) is tested.

## ビート，メロディ動画生成
- Java SE Development Kit 8 (JDK1.8.0_261)

Environments under [Java Extension Pack in Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack/) is tested.
- Windows Power Shell 5.1


# Note
 
I don't test environments under Linux and Mac.