from src.with_hpss import with_hpss
from src.output import Output
from glob import glob
import time
import os


## simulation types
song_length = -1      ## trimming songs from 0 to this sec.  if -1, using full
detect_type = "stft"  ## "moving" or "stft"
filter_type = "hpf"   ## "hpf", "lpf" or "none"

## directory names
data_dir = "input_signal"                       ## mp3 data directory
tempo_dir = "tempo"                             ## tempo.csv directory
visualize_dir = "processingAnimation/src/data"  ## output for processing directory
result_dir = "beat_melody_result"               ## output of detection directory


def mkdir(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)


if __name__ == "__main__":
    t_start = time.time()
    mkdir(visualize_dir)
    mkdir(result_dir)

    with open(visualize_dir + "./test_songs.txt", "w") as f:
        pass
    
    signal_files = glob(data_dir + "/*.mp3")
    if len(signal_files) == 0:
        print("No files")
    else:
        file_num = len(signal_files)
        detector = with_hpss(data_dir=data_dir, visu_dir=visualize_dir,
                             detect_type=detect_type, filter_type=filter_type,
                             use_tempo=tempo_dir)

        for i, sig_file in enumerate(signal_files):
            filename = sig_file.split("\\")[-1].split(".")[0]
            with open(visualize_dir + "/test_songs.txt", "a") as f:
                f.write(f"{filename}\n")
            print(f"{i+1:02}/{file_num:02}: {filename}")

            ## detect and get result
            o, plot_data, labels, song_length = detector.extract_beat(song_length=song_length, 
                                                                      result_dir=result_dir, 
                                                                      filename=filename)

            ## output for csv
            o.for_visualize(*plot_data, labels=labels, save_dir=result_dir, savename=filename)
            ## output for processing
            o.for_visualize(*plot_data, song_length=min(song_length, 30), labels=labels, 
                            save_dir="./"+visualize_dir, savename=filename)
            
    t_end = time.time()
    print(f"time:{t_end - t_start}[s]")
