import java.util.*;
import processing.core.*;

public class BeatWithWave extends PApplet {
    String beatOrMelody;
    String songname;
    ArrayList<Float> beat = new ArrayList<Float>();
    int fps = 30;
    // String added = "_tempo_hpss";
    // String songname = "brazilsamba";
    String datadir = "data/";
    float time;
    float d;
    int t;
    int data_i;
    int next_draw;
    int keep_draw = 0;
    PImage img;

    @Override
    public void settings()
    {
        this.size(800, 800);
    }
    
    @Override
    public void setup()
    {
      surface.setVisible(false); 
      String args[] = split(loadStrings("./setting")[0],',');
      beatOrMelody = args[0].trim();
      songname = args[1].trim();
      time = Float.parseFloat(args[2].trim());
      System.out.println(songname);
      // datadir += added.substring(1, added.length()) + "/";
      noStroke();
      colorMode(RGB,256, 256, 256, 100);
      rectMode(CORNERS);
      // frameRate(fps);
      frameRate(600);
      data_i = 0;
      t = 0;
      
      // String csvLine[] = loadStrings("./" + datadir + songname + added + ".csv");
      String csvLine[] = loadStrings("./" + datadir + songname + ".csv");
      String csvData[] = Arrays.copyOfRange(csvLine, 1, csvLine.length);
      for(String l: csvData){
        // System.out.println(l);
        String[] l_spl = l.split(",", 0);
        if(l_spl[1].equals(beatOrMelody)){            
          beat.add(Float.parseFloat(l_spl[0]));
        }
      }
      if(beat.size()!=0){
        next_draw = (int)(beat.get(data_i)*fps);
      }else{
        next_draw = -1;
      }
      
      img = loadImage("./" + datadir + songname + "_waveline_" + beatOrMelody + ".png");
    }

    @Override
    public void draw() 
    { 
      background(0);
      image(img, 0, width*3/4, width, width/4);
      fill(127,127,127,80);
      rect((float)(width*(0.075+0.865*t/fps/time)), (float)(height*0.795), (float)(width*0.94), (float)(height*0.9));
      
      if(t == next_draw){
        keep_draw = (int)(fps/10);
        data_i++;
        if(data_i < beat.size()){
          next_draw = (int)(beat.get(data_i)*fps);
        }
      }

      if(keep_draw > 0){
        keep_draw--;
        fill(255);
        ellipse(width/2, height*3/4/2, width/2, height/2);
      }
      if(t == (int)(time*fps)){
        exit();
      }
      t++;
      
      saveFrame("./src/" + datadir + "frames/######.tif");
    }

    public static void main(String args[]) {
        PApplet.main("BeatWithWave");
    }
}
