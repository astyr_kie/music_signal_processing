﻿# $this_simuration = "stft_hpss/"
$this_simuration = ""
$song_length = "30"
$beat_melody = @("BEAT", "MELODY")

# python .\beat_harmony_detection.py $this_simuration $song_length

cd .\processingAnimation

$songs = (Get-Content .\src\data\test_songs.txt) -as [string[]]
$outputdir = ".\output\" + $this_simuration
if(-! (Test-Path $outputdir)){
    New-Item $outputdir -ItemType Directory
}

javac -cp .\lib\core.jar .\src\BeatWithWave.java

foreach($song in $songs){
    $imagefile = ".\src\data\"+$this_simuration+"frames\%6d.tif"
    $musicfile = ".\src\data\"+$this_simuration+""+$song+"_trimed.mp3"
    foreach($b_or_m in $beat_melody){
        $outputfile = ".\"+$outputdir+"\"+$song+"_"+$b_or_m+".mp4"
        $b_or_m + "," + $song + "," + $song_length | Out-file -Encoding default -filepath .\src\setting -width 1000
        cd src
        java -cp ".;..\lib\core.jar" BeatWithWave
        cd ..

        ffmpeg -y -framerate 30 -i $imagefile -i $musicfile -ac 44100 -vcodec libx264 -pix_fmt yuv420p -r 30 -shortest $outputfile
        
        $imagefile_wild = $imagefile.Replace("%6d", "*")
        Remove-Item $imagefile_wild
    }
}


cd ..