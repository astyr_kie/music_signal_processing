import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import librosa


class MovingAve(object):
    """ 
    beat/harmony detection by moving average method
    """

    def __init__(self, fs):
        ## frequency of signal
        self.fs = fs


    def count_beat(self, diffs, diff_thr, min_interval):
        fs = self.fs
        ## moving range
        mv_range = fs//20
        beat = []
        prev_time = -100*min_interval*fs
        for i, diff in enumerate(diffs):
            t = i*fs/100
            if diff > diff_thr and t - prev_time > min_interval*fs:
                prev_time = t
                beat.append((t+mv_range//2)/fs)
        
        return beat


    def bi_search(self, diffs, beat_num, min_thr, max_thr, toler, min_interval):
        mid_thr = (min_thr+max_thr)/2
        beat = self.count_beat(diffs, mid_thr, min_interval)
        # print(f"(min, max):({min_thr:.4f},{max_thr:.4f}) beat:{len(beat)}/{int(beat_num)}")
        if max_thr - min_thr < 1e-3:
            return beat, mid_thr

        if beat_num*(1-toler) < len(beat) < beat_num*(1+toler):
            return beat, mid_thr
        elif len(beat) <= beat_num*(1-toler):
            return self.bi_search(diffs, beat_num, min_thr, mid_thr, toler, min_interval)
        elif beat_num*(1+toler) <= len(beat):
            return self.bi_search(diffs, beat_num, mid_thr, max_thr, toler, min_interval)
        else:
            return beat, mid_thr


    def culc_diff(self, inputSignal, diff_thr=-1, min_interval=0.1):  # diff_thr == -s: adaptive threshold with s/2~s seconds 
        fs = self.fs
        mv_range = fs//20
        mv_interval = fs//100
        sig_len = len(inputSignal)

        beat = []
        prev_segment = 0
        # diff of moving avarage from prev
        diffs = np.zeros(sig_len//mv_interval+1)
        for i, t in enumerate(range(0, sig_len, mv_interval)):
            # print(i, t)
            now_segment = np.sum(np.abs(inputSignal[t:t+mv_range]))
            diffs[i] = now_segment - prev_segment
            prev_segment = now_segment

        if diff_thr > 0:
            beat = self.count_beat(diffs, diff_thr, min_interval)
        else:
            ## adaptive threshold
            beat_num = int(sig_len/fs/(-diff_thr))
            toler = 0.1
            beat, ada_thr = self.bi_search(diffs, beat_num, max(np.min(diffs), 0), np.max(diffs), toler, min_interval)
            # print(f"ada_thr:{ada_thr}")
        
        t2 = np.linspace(0, sig_len / fs, sig_len//mv_interval+1)
        return t2, diffs, beat


    def with_tempo(self, inputSignal, bps=None, tempo=None, min_interval=0.5, toler = 0.1):
        fs = self.fs
        mv_range = fs//20
        mv_interval = fs//100
        sig_len = len(inputSignal)

        beat = []
        prev_segment = 0
        # diff of moving avarage from prev
        diffs = np.zeros(sig_len//mv_interval+1)
        for i, t in enumerate(range(0, sig_len, mv_interval)):
            # print(i, t)
            now_segment = np.sum(np.abs(inputSignal[t:t+mv_range]))
            diffs[i] = now_segment - prev_segment
            prev_segment = now_segment
    
        ## adaptive threshold
        if tempo:
            beat_num = (tempo/60/2)*sig_len/fs  ## beat per 2 quarters
            min_interval = 0.9*60/tempo         ## interval of a quarter
            # print(f"tempo:{tempo} beatnum:{beat_num} min_interval:{min_interval}")
        else:
            beat_num = bps*sig_len/fs
        beat, ada_thr = self.bi_search(diffs, beat_num, max(np.min(diffs), 0), np.max(diffs), toler, min_interval)
        # print(f"ada_thr:{ada_thr}")
        
        t2 = np.linspace(0, sig_len / fs, sig_len//mv_interval+1)
        return t2, diffs, beat


class Stft(object):
    """ 
    beat/harmony detection by STFT method
    """

    def __init__(self, fs):
        ## frequency of signal
        self.fs = fs


    def count_beat(self, diffs, diff_thr, min_interval):
        ## detect each diffs which is over the threshold(diff_thr)
        fs = self.fs
        beat = []
        t_interval = fs//20//128+1 
        prev_time = -100*min_interval*fs
        prev_i = 0
        for i, diff in enumerate(diffs):
            t = ((i+0.5)*t_interval)*128/fs
            if diff > diff_thr:
                if t - prev_time > min_interval:
                    prev_time = t
                    prev_i = i
                    beat.append(t)
                elif diffs[prev_i] < diff:
                    beat.pop(-1)
                    beat.append(t)

        return beat

        
    def bi_search(self, diffs, beat_num, min_thr, max_thr, toler, min_interval):
        ## search the best threshold that detect appropriate num of beat/harmony

        mid_thr = (min_thr+max_thr)/2
        beat = self.count_beat(diffs, mid_thr, min_interval)
        # print(f"(min, max):({min_thr:.4f},{max_thr:.4f}) beat:{len(beat)}/{int(beat_num)}")
        if max_thr - min_thr < 1e-3:
            return beat, mid_thr

        if beat_num*(1-toler) < len(beat) < beat_num*(1+toler):
            return beat, mid_thr
        elif len(beat) <= beat_num*(1-toler):
            return self.bi_search(diffs, beat_num, min_thr, mid_thr, toler, min_interval)
        elif beat_num*(1+toler) <= len(beat):
            return self.bi_search(diffs, beat_num, mid_thr, max_thr, toler, min_interval)
        else:
            return beat, mid_thr


    def culc_diff(self, abs_z, t_interval):
        ## culculate diffs(novelty function)
        t_len = abs_z.shape[1]
        prev_segment = np.zeros((abs_z.shape[0], t_interval))
        diffs = np.zeros(t_len//t_interval+1)
        t2 = []
        for i, t in enumerate(range(0, t_len, t_interval)):
            t2.append((t+t_interval*0.5)*128/self.fs)
            now_segment = abs_z[:, t:t+t_interval]
            diffs[i] = np.sum(now_segment - prev_segment[:, :now_segment.shape[1]])
            prev_segment = now_segment.copy()

        return t2, diffs


    def with_tempo(self, inputSignal, bps=None, tempo=None, min_interval=0.5, toler = 0.1):
        fs = self.fs
        sig_len = len(inputSignal)
        f, t, zxx = signal.stft(inputSignal, fs)
        abs_z = np.abs(zxx)
        t_interval = fs//20//128+1  #
        t2, diffs = self.culc_diff(abs_z, t_interval)

        ## adaptive threshold
        if tempo:
            beat_num = (tempo/60/2)*sig_len/fs  ## beat per 2 quarters
            min_interval = 0.9*60/tempo         ## interval of a quarter
            # print(f"tempo:{tempo} beatnum:{beat_num} min_interval:{min_interval}")
        else:
            beat_num = bps*sig_len/fs
        # print(tempo, beat_num)
        beat, ada_thr = self.bi_search(diffs, beat_num, max(np.min(diffs), 0), np.max(diffs), toler, min_interval)

        return np.array(t2), diffs, beat


if __name__ == "__main__":
    [inputSignal, fs] = librosa.core.load("./input_signal/bensound-punky.wav", sr=None)
    a = Stft(fs).with_tempo(inputSignal, 2, tempo=216)
    print(a)