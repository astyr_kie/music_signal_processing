import numpy as np
from scipy import signal
from matplotlib import pyplot as plt

class Filter(object):
    def __init__(self, fs, f_pass, f_stop):
        # パラメータ設定
        self.fs = fs
        self.f_pass = f_pass        # 通過域端周波数[Hz]
        self.f_stop = f_stop        # 阻止域端周波数[Hz]
        self.gpass = 1                   # 通過域最大損失量[dB]
        self.gstop = 40                  # 阻止域最小減衰量[dB]
        # 正規化
        self.Wp = f_pass/(fs//2)
        self.Ws = f_stop/(fs//2)

    def plot_freq(self, b, a=1, fil_name=""):
        w, h = signal.freqz(b, a)
        amp = 20 * np.log10(abs(h))
        f = w / (2 * np.pi) * self.fs
        plt.plot(f, amp)
        plt.title(f"Frequency response {fil_name} {self.f_pass}Hz")
        plt.xscale("log")
        plt.yscale("symlog")
        plt.ylim([-50, 0.1])
        plt.xlabel("frequency [Hz]")
        plt.ylabel("Magnitude [dB]")
        plt.show()


    def ellip(self, signal_in, firnum=51, fil_type="low"):
        N, Wn = signal.ellipord(self.Wp, self.Ws, self.gpass, self.gstop)
        b, a = signal.ellip(N, self.gpass, self.gstop, Wn, btype=fil_type)
        signal_out = signal.filtfilt(b, a, signal_in)

        # self.plot_freq(b, a, fil_name="Ellip-"+fil_type)

        return signal_out
    

    def fir(self, signal_in, firnum=51, pz=True):
        signal_out = np.zeros_like(signal_in)
        a = 1
        b = signal.firwin(firnum, self.Wp, window="hann", pass_zero=pz)
        delay = (firnum-1)//2
        signal_out[:-delay] = signal.lfilter(b, a, signal_in)[delay:]

        fil_type = "low" if pz else "high"
        # self.plot_freq(b, a=1, fil_name="FIR-"+fil_type)

        return signal_out
