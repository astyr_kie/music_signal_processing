import pydub

'''


'''

class MusicConvert(object):
    def __init__(self, directory=None) -> None:
        if directory:
            self.dir = directory + "/"
        else:
            self.dir = ""

    def wavtomp3(self, filename, export_name, time=None, export_dir=None):
        if export_dir is None:
            export_dir = self.dir
        else:
            export_dir += "/"
        sound = pydub.AudioSegment.from_wav("./" + self.dir + filename + ".wav")
        if time:
            sound = sound[:int(time*1000)]
        sound.export("./" + export_dir + export_name + ".mp3", format="mp3")
        
    def mp3towav(self, filename, export_name, time=None, export_dir=None):
        if export_dir is None:
            export_dir = self.dir
        else:
            export_dir += "/"
        sound = pydub.AudioSegment.from_file("./" + self.dir + filename + ".mp3", format="mp3")
        if time:
            sound = sound[:int(time*1000)]
        sound.export("./" + export_dir + export_name + ".wav", format="wav")

if __name__ == "__main__":
    wtm = MusicConvert("signal")
    wtm.mp3towav("sample3", "sample3", 15.0)
