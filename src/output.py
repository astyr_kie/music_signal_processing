import numpy as np
import matplotlib.pyplot as plt
import librosa
import librosa.display
import csv
import os

from scipy import signal

eps = 1e-10

class Output(object):
    def __init__(self, filename, converter, fs):
        self.filename = filename
        self.converter = converter
        self.fs = fs


    def write_csv(self, filepath,  data, label=None):
        with open(filepath, "w", newline="") as f:
            writer = csv.writer(f)
            if label:
                writer.writerow(label)
            for row in zip(*data):
                print(row)
                writer.writerow(row)


    def beat_plot(self, ax, t, diff, beat, label=None, y_min=None):
        if y_min is None:
            y_min = -np.max(diff)/100
        ax.plot(t, diff)
        for b in beat:
            # plt.plot([b, b], [-h, h], '--r')
            ax.plot([b, b], [y_min, np.max(diff)], '--r')
        ax.set_ylim(y_min, np.max(diff))
        if label:
            ax.set_ylabel(label)


    def for_visualize(self, t, signal, beats, beats_sub, song_length=None, labels=None, save_dir=None, savename=None, added=""):
        if save_dir is None:
            save_dir = "beat_with_wave/data"
        if savename is None:
            songname = ""
            savename = "beat_time"
        else:
            songname = savename + "_"

        if song_length:
            ## for processing
            self.converter.wavtomp3(self.filename, songname+"trimed", song_length, export_dir=save_dir)
            for beat_sub, label in zip(beats_sub, labels):        
                fig = plt.figure(figsize=(4.0, 1.0))
                ax = fig.add_subplot(1,1,1)
                self.beat_plot(ax, t[:song_length*self.fs:50], signal[:song_length*self.fs:50], beat_sub, y_min=np.min(signal))
                ax.set_xlim(0, song_length)
                ax.set_position([0.1,0.1,0.8,0.8])
                ax.axes.yaxis.set_visible(False)

                plt.grid(True)
                plt.tight_layout()
                plt.savefig("./" + save_dir + "/" + savename + added + f"_waveline_" + label)
                # plt.show()
                plt.clf()
                plt.close()
            
            with open("./" + save_dir + "/" + savename + added + ".csv", "w", newline="") as f:
                writer = csv.writer(f)
                writer.writerow(["イベント時刻", "BEAT/MELODY区分"])
                for i, beat in enumerate(beats_sub):
                    for b in beat:
                        writer.writerow([f"{b:.3f}", labels[i]])
        else:
            ## for output csv
            with open("./" + save_dir + "/" + savename + added + ".csv", "w", newline="") as f:
                writer = csv.writer(f)
                writer.writerow(["イベント時刻", "BEAT/MELODY区分"])
                for i, beat in enumerate(beats):
                    for b in beat:
                        writer.writerow([f"{b:.3f}", labels[i]])


    def stft_plot(self, fs, signal, fig, ax, title=None):
        f, t, x = signal.stft(signal)
        x_amp = np.abs(x)
        print(t, f.shape, x.shape)
        pcm = ax.pcolormesh(t/fs, f*fs, np.log(x_amp+eps), cmap="rainbow")    
        if title:
            ax.set_title(title)
        # plt.ylim([10, fs//2])
        # plt.yscale("log")
        # librosa.display.specshow(librosa.amplitude_to_db(np.abs(x_amp), ref=np.max), y_axis='log')
        fig.colorbar(pcm, ax=ax)
        # plt.colorbar(format='%+2.0f dB')


    def colormap_plot(self, z, x, y, title=None):
        if title:
            plt.figure(title)
        plt.pcolormesh(x, y, z, cmap="rainbow")
        if title:
            plt.title(title)
        plt.colorbar()
        plt.show()


if __name__ == "__main__":
    [inputSignal, fs] = librosa.core.load("./input_signal/bensound-love.wav", sr=None)

    f, t, x = signal.stft(inputSignal)
    x_amp = np.abs(x)
    print(t.shape, f.shape, x.shape)
    plt.pcolormesh(t/fs, f*fs, np.log(x_amp+eps), cmap="rainbow")    
    plt.ylim([10, fs//2])
    plt.colorbar()
    plt.show()