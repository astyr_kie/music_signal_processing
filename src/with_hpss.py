import librosa
import matplotlib.pyplot as plt
from .detect import MovingAve, Stft
from .music_convert import MusicConvert
from .filter import Filter
from .output import Output
import numpy as np
import soundfile as sf
import os
import warnings
import csv


class with_hpss(object):
    def __init__(self, data_dir, visu_dir, detect_type, filter_type, use_tempo=None):
        tempo_dict = {}
        self.data_dir = data_dir
        self.visu_dir = visu_dir
        self.detect_type = detect_type
        self.filter_type = filter_type

        if use_tempo and os.path.isfile("./" + use_tempo + "/tempo.csv"):
            print("tempo uses")
            #input tempo data
            with open("./" + use_tempo + "/tempo.csv", "r") as f:
                reader = csv.reader(f)
                for i, row in enumerate(reader):
                    if i != 0:
                        tempo_tmp = int(row[1])
                        if tempo_tmp > 180:
                            tempo_tmp //= 2
                        tempo_dict[row[0]] = tempo_tmp
            print(tempo_dict)
        self.tempo_dict = tempo_dict
    

    def extract_beat(self, song_length=15, result_dir=None, filename="sample2", ex_tag=""):
        
        warnings.simplefilter('ignore', UserWarning)
        converter = MusicConvert(directory=self.data_dir)

        ## convert wav to mp3 if not exist
        if not os.path.isfile("./" + self.data_dir + "/" + filename + ".wav"):
            if os.path.isfile("./" + self.data_dir + "/" + filename + ".mp3"):
                converter.mp3towav(filename, filename)
            else:
                print("file not exist")
                exit(1)

        ## load signal and frequency
        [inputSignal, fs] = librosa.core.load("./" + self.data_dir + "/" + filename + ".wav", sr=None)
        if song_length == -1:
            song_length = inputSignal.shape[0]//fs
        inputSignal = inputSignal[0: int(song_length * fs)]
        # inputSignal = inputSignal[:, 1]

        t = np.linspace(0, len(inputSignal) / fs, len(inputSignal))
        o = Output(filename, converter, fs)

        if self.detect_type == "moving":
            detect = MovingAve(fs)
        elif self.detect_type == "stft":
            detect = Stft(fs)
        else:
            print("Error: invalid detect_type")
            exit(1)

        ## hpss
        sig_harm, sig_perc = librosa.effects.hpss(inputSignal)


        signals = [sig_harm, sig_perc]
        labels = ["MELODY", "BEAT"]
        diffs = []
        beats = []

        t2 = 0
        for i, sig in enumerate(signals):
            if self.filter_type == "hpf":
                ## high-pass
                f_pass = 800  # [Hz]
                fil = Filter(fs, f_pass, f_pass//2)
                # filtered_signal = fil.fir(signal, firnum = 127, pz=False)
                filtered_signal = fil.ellip(sig, firnum=127, fil_type="high")
            elif self.filter_type == "lpf":
                ## low-pass
                f_pass = 200  # [Hz]
                fil = Filter(fs, f_pass, f_pass*2)
                # filtered_signal = fil.fir(signal, firnum = 127, pz=True)
                filtered_signal = fil.ellip(sig, firnum=127, fil_type="low")
            else:
                ## no filter
                filtered_signal = sig

            # sf.write("./" + result_dir + "/no_filter/" + filename + f"_{labels[i]}_{self.filter_type}.wav", 
            #          filtered_signal, fs)

            ## detect beat 
            if labels[i] == "BEAT":
                tempo = self.tempo_dict.get(filename)
            else:
                ## labels == "MELODY"
                tempo = self.tempo_dict.get(filename)*2
            t2, diff, beat = detect.with_tempo(filtered_signal, tempo=tempo)
            diffs.append(diff)
            beats.append(beat)

        ## beats trimed in partial length
        beats_sub = []
        for i in range(len(signals)):
            j = 0
            while j < len(beats[i]) and beats[i][j] < song_length:
                j += 1
            beats_sub.append(beats[i][:j])
        
        ## output results of detection by .png
        fig = plt.figure(filename)
        for i in range(len(signals)):
            ax2 = fig.add_subplot(len(signals), 1, i+1)
            o.beat_plot(ax2, t2, diffs[i], beats[i], labels[i])
            # o.beat_plot(ax2, t[:song_length*fs:50], inputSignal[:song_length*fs:50], beats_sub[i], labels[i], y_min=np.min(inputSignal[:song_length*fs:50]))
            ax2.axes.yaxis.set_ticks([])
            if i==0:
                plt.title("novelty function")
        # plt.tight_layout()
        # plt.show()
        plt.savefig("./" + result_dir + "/" + filename + ex_tag + ".png")
        plt.clf()
        plt.close()

        return o, (t, inputSignal, beats, beats_sub), labels, song_length