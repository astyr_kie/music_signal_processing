import librosa
import numpy as np
import scipy.signal
from src.output import Output
from src.detect import Stft
from src.music_convert import MusicConvert
import matplotlib.pyplot as plt
from glob import glob
import os
import csv

eps = 1e-10
bpm_min = 20
bpm_max = 360
oct_bin = 36

pendulum_step = np.concatenate([np.arange(20, 60, 2), 
                                np.arange(60, 72, 3), 
                                np.arange(72, 120, 4), 
                                np.arange(120, 144, 6), 
                                np.arange(144, 241, 8)])


def csv_output(song_name_list, tempo_list):
    with open("./tempo/tempo.csv", "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(["曲名", "BPM値"])
        for song, tempo in zip(song_name_list, tempo_list):
            writer.writerow([song, tempo])


def pend_tempo(tempo_in):
    i = 1
    while i < len(pendulum_step) and pendulum_step[i] < tempo_in:
        i += 1
    if abs(pendulum_step[i-1] - tempo_in) < abs(pendulum_step[i] - tempo_in):
        return pendulum_step[i-1]
    else:
        return pendulum_step[i]


def estimate_tempo(log_f, log_tg, song_name):
    f_len = log_f.shape[0]
    t_len = log_tg.shape[1]
    w1 = np.linspace(0, 1, (t_len+1)//2)
    if t_len %2 == 1:
        w = np.concatenate([w1[:-1], w1[::-1]])
    else:
        w = np.concatenate([w1, w1[::-1]])

    bpm_base = np.average(log_tg, axis=1, weights=w)
    bpm_score = np.zeros_like(log_f)

    for i in range(f_len):
        oct = i
        pow = 1
        while oct < f_len:
            bpm_score[i] += (bpm_base[oct])*pow
            oct += oct_bin
            pow /= 2
    
    tempo = log_f[np.argmax(bpm_score[1:-1])]*60
    # plt.plot(log_f*60, bpm_score)
    # plt.title(f"{song_name}: BPM{tempo}")
    # plt.show()

    return tempo

# def to_log(f, z, min_f=bpm_min/60, oct_bin=oct_bin, oct_num=math.log2(bpm_max/bpm_min)):
def to_log(f, z, min_f=bpm_min/60, oct_bin=oct_bin, oct_num=3.5):
    from scipy.interpolate import interp1d
    log_f = min_f * 2 ** (np.arange(0, oct_num*oct_bin)/oct_bin)
    log_z = interp1d(f, z, kind='linear', axis=0, fill_value='extrapolate')(log_f)
    
    return log_f, log_z 


def novelty_func(signal, fs, t_interval):    
    novelty = Stft(fs)
    zxx = np.abs(scipy.signal.stft(signal, fs)[2])
    t_interval = fs//20//128+1  #
    # print(f"t_interval{t_interval}")
    t2, diffs = novelty.culc_diff(zxx, t_interval)
    return t2, diffs


def f_tempogram(t2, diffs, t_interval):
    o = Output(None, None, None)

    f, t, x  = scipy.signal.stft(diffs, t_interval)
    min_i = -1
    max_i = f.shape[0] - 1
    while f[min_i+1] < bpm_min/60:
        min_i += 1
    while f[max_i-1] > bpm_max/60:
        max_i -= 1
    ftg = np.abs(x)[min_i:max_i]
    # print(f)
    f = f[min_i:max_i]
    log_f, log_ftg = to_log(f, ftg)

    fig = plt.figure(1)
    ax = fig.add_subplot(2, 1, 1)
    o.beat_plot(ax, t2, diffs, [])
    ax = fig.add_subplot(2, 1, 2)
    plt.pcolormesh(t, log_f*60, log_ftg, cmap="rainbow")    
    plt.colorbar()
    # o.stft_plot(log_f*60, log_ftg, fig, ax)
    plt.show()

    return log_f, log_ftg


def ac_tempogram(t2, diffs, t_interval):
    o = Output(None, None, None)

    nperseg = 256
    f, t3, z = scipy.signal.stft(diffs, t_interval, nperseg=nperseg)
    ps = np.abs(z) ** 2
    len_ps_f, len_ps_t = ps.shape
    # print(f, len_ps_f)
    # print(t3, len_ps_t)
    actg = np.zeros((len_ps_f-1, len_ps_t))
    for t in range(0, len_ps_t):
        actg[:,t] = np.fft.irfft(ps[:,t], axis=0)[1:len_ps_f]
    sr_novfunc = fs / (nperseg/2)
    actg_bpms = 60 * sr_novfunc / np.arange(1, len_ps_f)
    actg, actg_bpms = actg[::-1], actg_bpms[::-1]

    min_i = -1
    max_i = f.shape[0] - 1
    while f[min_i+1] < bpm_min/60:
        min_i += 1
    while f[max_i-1] > bpm_max/60:
        max_i -= 1
    actg = actg[min_i:max_i]
    f = f[min_i:max_i]
    log_f, log_actg = to_log(f, actg)

    # fig = plt.figure(1)
    # ax = fig.add_subplot(2, 1, 1)
    # o.beat_plot(ax, t2, diffs, [])
    # ax = fig.add_subplot(2, 1, 2)
    # plt.pcolormesh(t3, f[::-1]*60, actg, cmap="rainbow")    
    # plt.colorbar()
    # # o.stft_plot(log_f*60, log_ftg, fig, ax)
    # plt.show()

    return log_f, log_actg



if __name__ == "__main__":
    song_list = glob("./input_signal/*.mp3")

    if len(song_list) == 0:
        print("No files")
    else:
        song_name_list = []
        tempo_list = []
        for song in song_list:
            song_name = song.split(os.sep)[-1][:-4]
            if not os.path.isfile(song.replace(".mp3", ".wav")):
                MusicConvert(directory="input_signal").mp3towav(song_name, song_name)
            [inputSignal, fs] = librosa.core.load(song, sr=None)
            t_interval = fs//20//128+1  #
            print(song_name)
            song_name_list.append(song_name)

            t2, diffs = novelty_func(inputSignal, fs, t_interval)
            
            onset_env = librosa.onset.onset_strength(inputSignal, sr=fs)
            tempo = librosa.beat.tempo(onset_envelope=onset_env, sr=fs)
        #     log_f, log_tg = f_tempogram(t2, diffs, t_interval)
        #     # log_f, log_tg = ac_tempogram(t2, diffs, t_interval)
        #     tempo = estimate_tempo(log_f, log_tg, song_name)
            p_tempo = pend_tempo(tempo)
            print(p_tempo)
            tempo_list.append(p_tempo)

        csv_output(song_name_list, tempo_list)
